// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

const { getDB } = require('../../getDbConnection');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN QUERY selectAllEntriesQuery %%%%%%%%%%%%%%%%%%

// Función que se encarga de listar (en base a una keyword de búsqueda) todas las entradas, con sus respectivas fotos, en orden descendente de creación.

const selectAllEntriesQuery = async (keyword = '') => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      `
        SELECT e.id, e.created_at, e.place, e.description, e.user_id, u.email, u.name
        FROM entries AS e
        LEFT JOIN users AS u ON (u.id = user_id)
        WHERE e.description LIKE ?
        ORDER BY e.created_at DESC;
      `,
      [`%${keyword}%`]
    );

    let entriesWithPhotos = [];

    // Verificamos si tengo entries. Si entries.length > 0 siginifica que, al menos, hay una entry.
    // Mapeamos las entries y saco los id de cada una y los almaceno en la variable "arrayIDs":
    if (entries.length > 0) {
      const arrayIDs = entries.map((entry) => {
        return entry.id;
      });

      const [photos] = await connection.query(
        `
          SELECT *
          FROM photos;
        `
      );

      // Juntamos la entries con las photos en un array llamado "photoEntry":
      entriesWithPhotos = entries.map((entry) => {
        const photoEntry = photos.filter((photo) => {
          return photo.entry_id === entry.id;
        });
        return {
          ...entry,
          photos: photoEntry,
        };
      });

      return entriesWithPhotos;
    }
  } finally {
    if (connection) connection.release();
  }
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS selectAllEntriesQuery %%%%%%%%%%%%%%%%%%%%%%

module.exports = selectAllEntriesQuery;
