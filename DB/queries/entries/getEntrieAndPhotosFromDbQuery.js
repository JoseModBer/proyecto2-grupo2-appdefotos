// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% IMPORTACIONES NECESARIAS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%

const { generateError } = require('../../../helpers');

const { getDB } = require('../../getDbConnection');

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FUNCIÓN QUERY getEntrieAndPhotosFromDbQuery %%%%%%%%%%

// Función query que obtiene una entrada y sus fotos de la DB.

const getEntrieAndPhotosFromDbQuery = async (id) => {
  let connection;

  try {
    connection = await getDB();

    const [entries] = await connection.query(
      `
        SELECT id, created_at, place, description, user_id
        FROM entries
        WHERE id = ?
      `,
      [id]
    );

    if (entries.length < 1) {
      generateError('La entrada a buscar no existe.', 404);
    }

    const [photos] = await connection.query(
      `
        SELECT *
        FROM photos
        WHERE entry_id = ?
      `,
      [id]
    );

    return {
      ...entries[0],
      photo: photos,
    };
  } finally {
    if (connection) connection.release();
  }
};

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% EXPORTAMOS getEntrieAndPhotosFromDbQuery %%%%%%%%%%%%%

module.exports = getEntrieAndPhotosFromDbQuery;
